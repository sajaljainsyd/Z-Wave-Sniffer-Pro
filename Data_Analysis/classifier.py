import math
from IPython.display import set_matplotlib_formats, display
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from sklearn.linear_model import Perceptron
from sklearn import metrics
from sklearn.preprocessing import MinMaxScaler
from sklearn.svm import SVC
import argparse
import sys
from mpl_toolkits.mplot3d import Axes3D

def knn_train_test(feat, targ, test_perc, upper_k):

    # test_perc - percentage of test data / whole dataset 
    print("Performing KNN Classification on {:2.0f}%/{:2.0f}% split......"\
            .format(test_perc*100, (1-test_perc)*100))
 
    n_knn = list(range(3,upper_k))     # list of number of neightbours to test

    # split example datasets into training and testing (stratified)
    X_train, X_test, y_train, y_test = train_test_split(
        feat, targ, stratify=targ, test_size=test_perc)
        

    for k in n_knn:

        print("Results for {} nearest neighbours".format(k))

        # KNN Training
        knn = KNeighborsClassifier(n_neighbors=k)
        knn.fit(X_train, y_train)

        # KNN Testing
        y_pred = knn.predict(X_test)
        print("Test set predictions:\n", y_pred)

        acc = accuracy_score(y_test, y_pred)
        print("Accuracy on test set: {:.2f}".format(acc))

        #print(metrics.classification_report(y_test, y_pred))
        
        print("Confusion Matrix")
        print(metrics.confusion_matrix(y_test, y_pred))
        print("-----------------------------------------------")


    # Also use grid search to find out which distance measure
    # + number of nearest neighbours gives the best result
    print("Performing grid search .......")
    param_grid = {'n_neighbors': [1, 3, 5, 11, 15],
                  'p': [1, 2]}
    print("Parameter grid:\n{}".format(param_grid))
    grid_search = GridSearchCV(KNeighborsClassifier(), 
                    param_grid, cv=10, iid=False)
    grid_search.fit(X_train, y_train)

    print("Best parameters: {}".format(grid_search.best_params_))
    print("Best cross-validation score: {:.2f}".format(grid_search.best_score_))
    print("Best estimator:\n{}".format(grid_search.best_estimator_))
    print("Cross-validation results")
    print(pd.DataFrame(grid_search.cv_results_))

def gaus_nb(feat, targ, test_perc, ncv):
   

    # split example datasets into training and testing (stratified)
    X_train, X_test, y_train, y_test = train_test_split(
        feat, targ, stratify=targ, test_size=test_perc)
    
    # Gaussian NB algorithm with 10 folds stratified cross validation
    print()
    print("Performing Gaussian NB Classification with cross-validation.......")
    nb = GaussianNB()
    scores = cross_val_score(nb, X_train, y_train, cv=ncv)
    print("GaussianNB CV scores: {}".format(scores)) 
    print("Average CV score: {:.2f}".format(scores.mean()))

    # confusion matrix
    snb = GaussianNB()
    snb.fit(X_train, y_train)
    y_pred = snb.predict(X_test)
    print("Confusion Matrix")
    print(metrics.confusion_matrix(y_test, y_pred))
    
    return scores, scores.mean()


def svm(feat, targ, test_perc):

    print()
    # test_perc - percentage of test data / whole dataset 
    print("Performing SVM Classification on {:2.0f}%/{:2.0f}% split......"\
            .format(test_perc*100, (1-test_perc)*100))
    print("Please ensure normalisation is specified with SVM")
 
    X_train, X_test, y_train, y_test = train_test_split(
    feat, targ, stratify=targ, test_size=test_perc)

    para_grid = {'C': [0.001, 0.01, 0.1, 1, 10, 100],
                 'gamma': [0.001, 0.01, 0.1, 1, 10, 100]}

    rsvm = SVC(kernel="rbf")

    gs = GridSearchCV(rsvm, para_grid, cv=10,
                        iid=False, return_train_score=False)
    
    gs.fit(X_train, y_train)

    c = gs.best_params_['C']
    g = gs.best_params_['gamma']
    best = gs.best_score_
    tests = gs.score(X_test, y_test)
    
    print("Best Score: {:.2f} with C = {} & gamma = {}"\
        .format(best, c, g))

    print("Cross-validation results")
    print(pd.DataFrame(gs.cv_results_))

    print("Test set score: {:.2f}".format(tests))

    # Do a single run on the optimal hyper-parameters
    oC = 1
    ogam = 0.1
    ssvm = SVC(kernel='rbf', C=oC, gamma=ogam)
    ssvm.fit(X_train, y_train)
    y_pred = ssvm.predict(X_test)

    print("Confusion Matrix")
    print(metrics.confusion_matrix(y_test, y_pred))


def rounddown(x):
    return int(math.floor(x / 100.0)) * 100

def plot_feat_data(pzdata, fname, do3d):

    
    pzdf = pd.DataFrame(pzdata, columns=fname)
    fdim = pzdf[pzdf[fname[-1]] == 0]
    adim = pzdf[pzdf[fname[-1]] == 1]
    roll = pzdf[pzdf[fname[-1]] == 2]

    """
    alp = 0.4
    ax = smp.plot(x=fname[0], y=fname[1], label='Smart Plug',
                     kind='scatter', alpha=alp)
    fdim.plot(ax=ax, x=fname[0], y=fname[1], label='Fibaro dimmer',
                     kind='scatter', color='g', alpha=alp)
    adim.plot(ax=ax, x=fname[0], y=fname[1], label='Aeotec dimmer', 
                    kind='scatter', color='r', alpha=alp)
    """

    targ = pzdata[:,-1].astype(int)
    feat_df = pd.DataFrame(pzdata[:,:-1], columns=fname[:-1])
    ax = pd.plotting.scatter_matrix(feat_df, c=targ, figsize=(15, 15),
                            marker='o', hist_kwds={'bins': 20}, s=60,
                            alpha=.6);

    if do3d:
        fig = plt.figure()
        ax2 = plt.axes(projection='3d')
            
        ax2.scatter(feat[:, 0], feat[:, 1], feat[:, 2]*1000000, c=targ, 
                   cmap='viridis')
        
        ax2.set_xlim(0, 800)
        ax2.set_zlim(0, 80)
        ax2.set_xlabel("Packet Size (bytes)")
        ax2.set_ylabel("Number of Packet")
        ax2.set_zlabel("Packet Interval (microsec)")


    plt.show()

if __name__ == '__main__':

    parser = argparse.ArgumentParser( 
        description='Read Processed Z-Wave file and perform classification', 
        add_help=True) 
    parser.add_argument("-f", action="store", dest='file', 
        help="Processed Z-Wave file", required=True, type=str) 
    
    parser.add_argument("-algo", action="store", dest='algo', 
        help="Specified KNN/NB/SVM Algorithm", required=True, type=str) 

    parser.add_argument("-p", action="store", dest='perc', 
        help="Test/Train Data Percentage e.g. 0.2", nargs='?',
        default=0.2, type=float) 

    parser.add_argument("--norm", action="store_true", dest="donorm",
        help="Use this option to normalise the data first")
    parser.set_defaults(donorm=False) 
    
    parser.add_argument("--plot", action="store_true", dest="doplot",
        help="Use this option to plot the points onto a 2-D graph")
    parser.set_defaults(donorm=False) 
    
    parser.add_argument("--sc", action="store_true", dest="doscale",
        help="Use this option to scale the timestamp data")
    parser.set_defaults(doscale=False) 
        
    args = parser.parse_args()  

    try:
        pzdata = np.genfromtxt(args.file, delimiter=",",dtype=np.float)
    except IOError:
        print("FILE ERROR: Could not read from file " + args.file)
        sys.exit(0)

    feat = pzdata[:,:-1]
    targ = pzdata[:,-1].astype(int)

    fname = ['packet_size (bytes)', 'num_of_packet', 'packet_interval (secs)', 'class']

    if args.donorm:

        # perform normalisation
        scaler = MinMaxScaler()
        scaler.fit(feat)
        feat =  scaler.transform(feat)
        print("Normalised Data: ")
        print(feat)

    if args.doscale:

        sc = 10000
        print("Scaling the timestamp data by " + str(sc))
        feat[:, -1] = feat[:, -1] * sc
        print("After Scaling....... feat = ")
        print(feat)


    if args.doplot:

        plot_feat_data(pzdata, fname, True)


    if args.perc<=0 or args.perc>=1:
        print("Please specify a percentage between 0 and 1")
        sys.exit(0)

    algo = args.algo.lower()
    
    pd.set_option('display.max_columns', None)
   
    if algo == 'nb':
        gaus_nb(feat, targ, args.perc, 10)
    elif algo == 'knn':
        knn_train_test(feat, targ, args.perc, 11)
    elif algo == 'svm':
        svm(feat, targ, args.perc)
    elif algo == 'ann':
        ann(feat, targ, args.perc, 2, 50)
    
    
