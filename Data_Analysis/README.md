# Encrypted Z-Wave Packets Preprocessing & Classifying scripts 


The preprocessing.py script uses the GNU Radio captured packets, extract the following three features and write it into an output file:
  
  - Total packet size
  - Number of packets
  - Packet Intervals

The classifier.py script uses the extracted feature file and perform machine learning algorithms to classify the encrypted devices.

# Prerequisite

  - Python 3.6 or newer
  - pandas module 0.25.1
  - numpy module 1.17.2 
  - sklean module 0.21.3

# Installation
No installation is required. 

# Basic Usage

```
usage: preprocessing.py [-h] -f FILE -tf TFILE -o OUTFILE -l LABEL
Read GNU Radio output file and print out packet summary
optional arguments:
  -h, --help  show this help message and exit
  -f FILE     Sample data file to read
  -tf TFILE   Timestamp file to read
  -o OUTFILE  Output Processed Data File
  -l LABEL    Specify an integer for your data label
```
```
usage: classifier.py [-h] -f FILE -algo ALGO [-p [PERC]] [--norm] [--plot] [--sc]
Read Processed Z-Wave file and perform classification
optional arguments:
  -h, --help  show this help message and exit
  -f FILE     Processed Z-Wave file
  -algo ALGO  Specified KNN/NB/SVM Algorithm
  -p [PERC]   Test/Train Data Percentage e.g. 0.2
  --norm      Use this option to normalise the data first
  --plot      Use this option to plot the points onto a 2-D graph
  --sc        Use this option to scale the timestamp data
```

# Examples
The following commands use the preprocessing.py script to read the GNU Radio packet file _gnu_sample_pk_ and the GNU Radio time file _gnu_sample_time_, extract the 3 features and write to _outfile.csv_ with a label of 1. 
```bash
$ python preprocessing.py -f gnu_sample_pk -tf gnu_sample_time -l 1 -o outfile.csv
```

To add another label (device) to _outfile.csv_, simply specify the new packet file and provide a different label. (e.g. label=2 in this case)
```bash
$ python preprocessing.py -f gnu_sample2_pk -tf gnu_sample2_time -l 2 -o outfile.csv
```
Use the classifier.py script to perform device classifications. The following command specifies the KNN algorithm with normalisation and provides graphs,
```bash
$ python classifier.py -f outfile.csv -algo knn --norm --plot
```


