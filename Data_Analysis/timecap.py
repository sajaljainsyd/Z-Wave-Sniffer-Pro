# run stdbuf -oL -eL gnuradio-companion | tee <file> | python timecap.py

import sys
import time;
ts = time.time()

pk_line = "* MESSAGE DEBUG PRINT PDU VERBOSE *"
nline = "----------NEXT-----------"

with open('packtime', 'a', buffering=1) as fout:
    for line in sys.stdin:
        if pk_line in line:
            ct = time.time()
            fout.write('{:f}\n'.format(ct))
        if nline in line:
            fout.write(nline+'\n')


