# This is used to pre-process the data once acquired
# class ZPacket is for non-encrypted data whereas
# class ENCPacket is for encrypted data

import sys
import argparse
import numpy as np
import matplotlib.pyplot as plt

class ZPacket:

    idcount = 0

    def __init__(self):
        self.crcdb = ""
        self.psize = 0
        self.ntwkid = ""
        self.src = ""
        self.dst = ""
        self.fc = ""
        self.fl = ""
        self.payload = ""
        self.crc = ""
        ZPacket.idcount += 1 
        self.id = ZPacket.idcount

    def extract_z_single_cast(self, conlist):

        byteseq = ""
        # first reconstruct into one line
        for line in conlist:
            byteseq = byteseq + line.split(': ')[1].rstrip("\n") + " "

        # put the seq into a list of bytes and remove the last space
        byteseq = byteseq.split(" ")[:-1]
        
        # remove first 8 bytes of GNU Radio headers
        byteseq = byteseq[8:]

        # first 4 bytes - Network ID
        self.ntwkid = ''.join(byteseq[0:4])
        # source ID
        self.src = byteseq[4]
        # frame control
        self.fc = ''.join(byteseq[5:7])
        # frame length
        self.fl = byteseq[7]
        # destination ID
        self.dst = byteseq[8]
        # encrpted payload
        self.payload = ''.join(byteseq[9:-1])
        # crc is the last byte
        self.crc = byteseq[-1]


    def display(self):
        print("-"*20 + "Captured Packet " +str(self.id) + "-"*20)
        print('{:<20s}{:<7s}'.format("Network ID: ", "0x" + self.ntwkid))
        print('{:<20s}{:<7s}'.format("Source ID: ", "0x" + self.src))
        print('{:<20s}{:<7s}'.format("Destination ID: ", "0x" + self.dst))
        print('{:<20s}{:<7s}'.format("Frame Length: ", "0x" + self.fl))
        print('{:<20s}{:<7s}'.format("Frame Control: ", "0x" + self.fc))
        print('{:<20s}{:<7s}'.format("Encrypted Payload: ", "0x" + self.payload))
        print('{:<20s}{:<7s}'.format("CRC: ", "0x" + self.crc))
        print('{:<20s}{:<7s}'.format("CRC Debug: ", "0x" + self.crcdb))


class ENCPacket:

    idcount = 1

    def __init__(self):
        self.plen = 0
        self.pslist = []    # list of intercepted packet sizes
        self.psum = 0       # sum of all the packet sizes
        self.pnum = 0       # number of intercepted packets of 1 operation
        self.id = ENCPacket.idcount

    def update_id(self):
        ENCPacket.idcount += 1

    def update_ps(self, ps):

        # remove 8 bytes of GNU header as it's not part of the packet
        ps_real = ps - 8

        self.pslist.append(ps_real)
        self.psum += ps_real
        self.pnum += 1

    def display(self):
        print("-"*20 + "Captured Packet " +str(self.id) + "-"*20)
        print('{:<22s}{:<5d}'.format("No. of Packets : ",
                                      self.pnum))
        print('{:<22s}{:<5d} bytes'.format("Total Packet Size: ", 
                                            self.psum))
        print('Packet Size Details: ', end=' ')
        print(self.pslist)
        print('-'*59)


    def get_ps_sum(self):
        return self.psum

    def get_ps_list(self):
        return self.pslist
    
    def get_pn(self):
        return self.pnum

def proc_gnu_enc_data(gdata, packetlist):

    sep = "-----NEXT-----"
    lenline = "pdu_length"
    
    i = 0
    while i < len(gdata):
        
        if sep in gdata[i]:
            
            encZP = ENCPacket()
            i += 1
            # now we sum up all the packets of 1 click
            # finish when the next line is ---------NEXT---------
            while i < len(gdata)-1 and not (sep in gdata[i+1]):
                
                # pdu_length line
                if lenline in gdata[i]:
                    lenval = gdata[i].split(' = ')[1].rstrip("\n")
                    if len(lenval) == 0:
                        lenval = '00'
                    encZP.update_ps(int(lenval,16))

                i += 1
            
            if encZP.pnum != 0:
                encZP.update_id()
                packetlist.append(encZP)
            
        i += 1

    print("Total Number of Captured Packets: " + str(len(packetlist))) 

     
def process_gnu_data(gdata, packetlist):

    seperator = "*********************"
    crcline = "CRC_debug"
    cline = "contents"    

    i = 0
    while i < len(gdata):
        
        if seperator in gdata[i]:
            newZP = ZPacket()
            i += 1
            #print(i)
            while i < len(gdata) and not (seperator in gdata[i]):
                
                # CRC_debug line
                if crcline in gdata[i]:
                    gcrcval = gdata[i].split(' : ')[1].rstrip("\n")
                    newZP.crcdb = gcrcval
                
                # contents line
                if cline in gdata[i]:
                    j = i+1
                    while not seperator in gdata[j]:
                        j += 1
                    conlist = gdata[i+1:j]
                    newZP.extract_z_single_cast(gdata[i+1:j])
                i += 1


            packetlist.append(newZP)

    print("Total Number of Captured Packets: " + str(len(packetlist)-1)) 

def proc_time_data(tdata, tlist):

    sep = "-----NEXT-----"
    i = 0

    while i < len(tdata):
        
        if sep in tdata[i]:
            
            # create a list to store all timestamps associated with 1 cmd
            ts_per_cmd = []

            # now we sum up all the packets of 1 click
            # finish when the next line is ---------NEXT---------
            while i < len(tdata)-1 and not (sep in tdata[i+1]):
                
                ts_per_cmd.append(float(tdata[i+1]))
                i += 1
            
            # finish one command 
            npk = len(ts_per_cmd)
    
            if npk == 1:
                # not enough packet to calculate interval
                tlist.append(-1.0)
            
            elif npk > 1:
                
                # calculate average packet interval
                pintlist = []
                for k in range(0, npk-1):
                    pint = abs(ts_per_cmd[k+1]-ts_per_cmd[k])
                    pintlist.append(pint)

                ave_pint = sum(pintlist)/len(pintlist)
                #ave_pint = pintlist[-1] - pintlist[0]
                tlist.append(ave_pint)
            
        i += 1
    print("Total Number of Captured Ave Ts: " + str(len(tlist))) 


# Currently, rm_corrupt_data is used instead of this function
def proc_missing_data(tlist, isrmout, isfill, newlist):

    count = 0
    tssum = 0
    tol = 0.01 

    i = 0
    # first remove outliers, the time different can't be > tol
    if isrmout:
        while i < len(tlist):
            if tlist[i] > tol:
                tlist[i] = -1
            i += 1
   

    # the outlier is so much larger than the actual data
    # so we can't choose the mean value to fill the mssing data
    # let's just use the first correct entry
    for ts in tlist:
        if ts < tol*0.01:
            fill = ts
            print("Filling outliers and missing data with " + 
                    str(fill))
            break
    
    # now fill the outlier and missing data        
    i = 0
    if isfill:
        while i < len(tlist):
            if tlist[i] == -1:
                tlist[i] = fill
                count += 1
            i += 1

        print("Number of missing data/outliers: " + str(count))


def rm_corrupt_data(tsl, psl, pnl):

    tol = 0.01
    missing = -1
    i = 0

    to_rm = len(tsl) - len(tsl[(tsl != -1) & (tsl < tol)])
    print("Removing {} missing data points".format(to_rm))
    
    pnl = pnl[(tsl != -1) & (tsl < tol)]
    psl = psl[(tsl != -1) & (tsl < tol)]
    tsl = tsl[(tsl != -1) & (tsl < tol)]

    return tsl, psl, pnl
    

if __name__ == "__main__":
    

    parser = argparse.ArgumentParser(
        description='Read GNU Radio output file and print out packet summary',
        add_help=True)
    parser.add_argument("-f", action ="store", dest='file',
        help="Sample data file to read", required=True) 
    parser.add_argument("-tf", action ="store", dest='tfile',
        help="Timestamp file to read", required=True) 
    
    parser.add_argument("-o", action ="store", dest='outfile',
        help="Output Processed Data File", required=True) 
    parser.add_argument("-l", action ="store", dest='label',
        help="Specify an integer for your data label", required=True) 
    args = parser.parse_args()   

    try:
        with open(args.file, 'r') as fp:
            gdata = fp.readlines()

    except OSError:
        print("Error: Cannot open or read from the file: " + args.file)
        sys.exit(0)

    try:
        with open(args.tfile, 'r') as tfp:
            tdata = tfp.readlines()

    except OSError:
        print("Error: Cannot open or read from the file: " + args.tfile)
        sys.exit(0)


    # For encrypted packets 
    plist = []
    tlist = []

    proc_gnu_enc_data(gdata, plist)
    proc_time_data(tdata, tlist)
    #proc_missing_data(tlist, isrmout, isfill, [])

    np_ts = np.array(tlist, dtype=float)
    np_pss = np.array([], dtype=int)
    np_psl = np.array([], dtype=int)
    np_pn = np.array([], dtype=int)

    for zp in plist:
        
        pss = zp.get_ps_sum()
        psl = zp.get_ps_list()
        pn = zp.get_pn()
        
        np_pss = np.append(np_pss, pss)
        np_psl = np.append(np_psl, psl)
        np_pn = np.append(np_pn, pn)


    np_ts, np_pss, np_pn = rm_corrupt_data(np_ts, np_pss, np_pn)

    # plot the data
    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.plot(np_pss, 'co', label='Packet Size')
    ax1.set_ylim(0, 350)
    ax3 = ax1.twinx()
    ax3.plot(np_pn, 'gx', label='No. of Packets')
    ax3.set_ylim(0, 25)

    ax1.legend(loc=2)
    ax3.legend(loc=1)

    ax1.set_title('Captured Packets per Command')
    ax1.set_xlabel('Command Number')
    #ax1.set_ylabel('Bytes')
    
    ax2.plot(np_ts, 'gx')
    ax2.set_title('Packet Interval')
    ax2.set_ylabel('secs')
    ax2.set_xlabel('Command Number')
    #ax2.set_ylim(0, 0.0006)
    

    # write the result to a file
    target = np.repeat(int(args.label), len(np_pss))
    comb = np.vstack((np_pss, np_pn, np_ts, target)).T
    try:
        with open(args.outfile, 'ab') as fout:
            np.savetxt(fout, comb, delimiter=',',fmt='%f')
    except OSError:
        print("Error: Cannot open or read from the file: " + args.file)
        sys.exit(0)
    
    plt.show()

    

