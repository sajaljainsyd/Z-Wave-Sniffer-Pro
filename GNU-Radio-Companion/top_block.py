#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Top Block
# GNU Radio version: 3.7.13.5
##################################################


if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from gnuradio import analog
from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import wxgui
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from gnuradio.wxgui import fftsink2
from gnuradio.wxgui import forms
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import SimpleXMLRPCServer
import Zwave
import math
import osmosdr
import threading
import time
import wx


class top_block(grc_wxgui.top_block_gui):

    def __init__(self):
        grc_wxgui.top_block_gui.__init__(self, title="Top Block")
        _icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
        self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 800000
        self.r2_baud = r2_baud = 40e3
        self.r1_freq_offset = r1_freq_offset = 8e4
        self.r1_baud = r1_baud = 19.2e3
        self.taps = taps = firdes.low_pass(1,samp_rate,samp_rate/(2*1), 10000)
        self.rx_if_gain = rx_if_gain = 24
        self.rx_bb_gain = rx_bb_gain = 24
        self.r2_sps = r2_sps = int(samp_rate/r2_baud)
        self.r2_freq_offset = r2_freq_offset = r1_freq_offset+2e4
        self.r1_sps = r1_sps = int(samp_rate/r1_baud)
        self.center_freq = center_freq = 921250000

        ##################################################
        # Blocks
        ##################################################
        _rx_if_gain_sizer = wx.BoxSizer(wx.VERTICAL)
        self._rx_if_gain_text_box = forms.text_box(
        	parent=self.GetWin(),
        	sizer=_rx_if_gain_sizer,
        	value=self.rx_if_gain,
        	callback=self.set_rx_if_gain,
        	label='rx_if_gain',
        	converter=forms.int_converter(),
        	proportion=0,
        )
        self._rx_if_gain_slider = forms.slider(
        	parent=self.GetWin(),
        	sizer=_rx_if_gain_sizer,
        	value=self.rx_if_gain,
        	callback=self.set_rx_if_gain,
        	minimum=0,
        	maximum=40,
        	num_steps=5,
        	style=wx.SL_HORIZONTAL,
        	cast=int,
        	proportion=1,
        )
        self.Add(_rx_if_gain_sizer)
        _rx_bb_gain_sizer = wx.BoxSizer(wx.VERTICAL)
        self._rx_bb_gain_text_box = forms.text_box(
        	parent=self.GetWin(),
        	sizer=_rx_bb_gain_sizer,
        	value=self.rx_bb_gain,
        	callback=self.set_rx_bb_gain,
        	label='rx_bb_gain',
        	converter=forms.int_converter(),
        	proportion=0,
        )
        self._rx_bb_gain_slider = forms.slider(
        	parent=self.GetWin(),
        	sizer=_rx_bb_gain_sizer,
        	value=self.rx_bb_gain,
        	callback=self.set_rx_bb_gain,
        	minimum=0,
        	maximum=62,
        	num_steps=31,
        	style=wx.SL_HORIZONTAL,
        	cast=int,
        	proportion=1,
        )
        self.Add(_rx_bb_gain_sizer)
        self.xmlrpc_server_0 = SimpleXMLRPCServer.SimpleXMLRPCServer(('localhost', 8080), allow_none=True)
        self.xmlrpc_server_0.register_instance(self)
        self.xmlrpc_server_0_thread = threading.Thread(target=self.xmlrpc_server_0.serve_forever)
        self.xmlrpc_server_0_thread.daemon = True
        self.xmlrpc_server_0_thread.start()
        self.wxgui_fftsink2_0_0 = fftsink2.fft_sink_f(
        	self.GetWin(),
        	baseband_freq=0,
        	y_per_div=10,
        	y_divs=10,
        	ref_level=30,
        	ref_scale=2.0,
        	sample_rate=samp_rate,
        	fft_size=1024,
        	fft_rate=15,
        	average=False,
        	avg_alpha=None,
        	title='FFT Plot',
        	peak_hold=False,
        )
        self.Add(self.wxgui_fftsink2_0_0.win)
        self.wxgui_fftsink2_0 = fftsink2.fft_sink_c(
        	self.GetWin(),
        	baseband_freq=921250000,
        	y_per_div=10,
        	y_divs=10,
        	ref_level=0,
        	ref_scale=2.0,
        	sample_rate=samp_rate,
        	fft_size=1024,
        	fft_rate=15,
        	average=False,
        	avg_alpha=None,
        	title='FFT Plot',
        	peak_hold=False,
        )
        self.Add(self.wxgui_fftsink2_0.win)
        self.osmosdr_source_0 = osmosdr.source( args="numchan=" + str(1) + " " + 'hackrf=9c6663' )
        self.osmosdr_source_0.set_sample_rate(samp_rate)
        self.osmosdr_source_0.set_center_freq(center_freq, 0)
        self.osmosdr_source_0.set_freq_corr(0, 0)
        self.osmosdr_source_0.set_dc_offset_mode(0, 0)
        self.osmosdr_source_0.set_iq_balance_mode(2, 0)
        self.osmosdr_source_0.set_gain_mode(False, 0)
        self.osmosdr_source_0.set_gain(10, 0)
        self.osmosdr_source_0.set_if_gain(rx_if_gain, 0)
        self.osmosdr_source_0.set_bb_gain(rx_bb_gain, 0)
        self.osmosdr_source_0.set_antenna('', 0)
        self.osmosdr_source_0.set_bandwidth(0, 0)

        self.low_pass_filter_0 = filter.fir_filter_fff(1, firdes.low_pass(
        	1, samp_rate, 100e3, 10e3, firdes.WIN_HAMMING, 6.76))
        self.freq_xlating_fir_filter_xxx_0 = filter.freq_xlating_fir_filter_ccc(1, (taps), -650000, samp_rate)
        self.digital_clock_recovery_mm_xx_0 = digital.clock_recovery_mm_ff(r2_sps, 0.25*0.175*0.175, 0.5, 0.175, 0.005)
        self.digital_binary_slicer_fb_0 = digital.binary_slicer_fb()
        self.blocks_message_debug_0 = blocks.message_debug()
        self.analog_simple_squelch_cc_0 = analog.simple_squelch_cc(-80, 1)
        self.analog_quadrature_demod_cf_0 = analog.quadrature_demod_cf(2*(samp_rate)/(2*math.pi*20e3/8.0))
        self.Zwave_packet_sink_0 = Zwave.packet_sink()



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.Zwave_packet_sink_0, 'out'), (self.blocks_message_debug_0, 'print'))
        self.connect((self.analog_quadrature_demod_cf_0, 0), (self.low_pass_filter_0, 0))
        self.connect((self.analog_simple_squelch_cc_0, 0), (self.analog_quadrature_demod_cf_0, 0))
        self.connect((self.digital_binary_slicer_fb_0, 0), (self.Zwave_packet_sink_0, 0))
        self.connect((self.digital_clock_recovery_mm_xx_0, 0), (self.digital_binary_slicer_fb_0, 0))
        self.connect((self.freq_xlating_fir_filter_xxx_0, 0), (self.analog_simple_squelch_cc_0, 0))
        self.connect((self.low_pass_filter_0, 0), (self.digital_clock_recovery_mm_xx_0, 0))
        self.connect((self.low_pass_filter_0, 0), (self.wxgui_fftsink2_0_0, 0))
        self.connect((self.osmosdr_source_0, 0), (self.freq_xlating_fir_filter_xxx_0, 0))
        self.connect((self.osmosdr_source_0, 0), (self.wxgui_fftsink2_0, 0))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_taps(firdes.low_pass(1,self.samp_rate,self.samp_rate/(2*1), 10000))
        self.set_r2_sps(int(self.samp_rate/self.r2_baud))
        self.wxgui_fftsink2_0_0.set_sample_rate(self.samp_rate)
        self.wxgui_fftsink2_0.set_sample_rate(self.samp_rate)
        self.set_r1_sps(int(self.samp_rate/self.r1_baud))
        self.osmosdr_source_0.set_sample_rate(self.samp_rate)
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, 100e3, 10e3, firdes.WIN_HAMMING, 6.76))
        self.analog_quadrature_demod_cf_0.set_gain(2*(self.samp_rate)/(2*math.pi*20e3/8.0))

    def get_r2_baud(self):
        return self.r2_baud

    def set_r2_baud(self, r2_baud):
        self.r2_baud = r2_baud
        self.set_r2_sps(int(self.samp_rate/self.r2_baud))

    def get_r1_freq_offset(self):
        return self.r1_freq_offset

    def set_r1_freq_offset(self, r1_freq_offset):
        self.r1_freq_offset = r1_freq_offset
        self.set_r2_freq_offset(self.r1_freq_offset+2e4)

    def get_r1_baud(self):
        return self.r1_baud

    def set_r1_baud(self, r1_baud):
        self.r1_baud = r1_baud
        self.set_r1_sps(int(self.samp_rate/self.r1_baud))

    def get_taps(self):
        return self.taps

    def set_taps(self, taps):
        self.taps = taps
        self.freq_xlating_fir_filter_xxx_0.set_taps((self.taps))

    def get_rx_if_gain(self):
        return self.rx_if_gain

    def set_rx_if_gain(self, rx_if_gain):
        self.rx_if_gain = rx_if_gain
        self._rx_if_gain_slider.set_value(self.rx_if_gain)
        self._rx_if_gain_text_box.set_value(self.rx_if_gain)
        self.osmosdr_source_0.set_if_gain(self.rx_if_gain, 0)

    def get_rx_bb_gain(self):
        return self.rx_bb_gain

    def set_rx_bb_gain(self, rx_bb_gain):
        self.rx_bb_gain = rx_bb_gain
        self._rx_bb_gain_slider.set_value(self.rx_bb_gain)
        self._rx_bb_gain_text_box.set_value(self.rx_bb_gain)
        self.osmosdr_source_0.set_bb_gain(self.rx_bb_gain, 0)

    def get_r2_sps(self):
        return self.r2_sps

    def set_r2_sps(self, r2_sps):
        self.r2_sps = r2_sps
        self.digital_clock_recovery_mm_xx_0.set_omega(self.r2_sps)

    def get_r2_freq_offset(self):
        return self.r2_freq_offset

    def set_r2_freq_offset(self, r2_freq_offset):
        self.r2_freq_offset = r2_freq_offset

    def get_r1_sps(self):
        return self.r1_sps

    def set_r1_sps(self, r1_sps):
        self.r1_sps = r1_sps

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        self.osmosdr_source_0.set_center_freq(self.center_freq, 0)


def main(top_block_cls=top_block, options=None):

    tb = top_block_cls()
    tb.Start(True)
    tb.Wait()


if __name__ == '__main__':
    main()
