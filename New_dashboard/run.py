from flask import Flask, render_template, jsonify, g, session
from random import *
from flask_cors import CORS
import requests
from flask import request
import sqlite3
import asyncio
import random
import os
import socket
import time
from threading import Thread
import threading
import csv
import datetime
from collections import defaultdict
import json


APP_ROOT = os.path.dirname(os.path.abspath(__file__))
app = Flask(__name__,
            static_url_path='', 
            static_folder="./frontend/dist",
            template_folder="./frontend/dist")
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})


# This Class Listens for the events from the GNURadio

class EchoServerProtocol:
    def __init__(self, data):
        threading.Thread.__init__(self)
        self._dbkey = data
        self._running = True

    def connection_made(self, transport):
        self.transport = transport
        query = ("CREATE TABLE packet_info ( id integer primary key, home_id VARCHAR(20), src_id VARCHAR(20), frame_control VARCHAR(20), length VARCHAR(20), commands VARCHAR(100), destination VARCHAR(20), command_class VARCHAR(20), timestamp VARCHAR(50) );")
        query_db(query, database='database/database'+str(self._dbkey)+'.db')
    # process and store the packet data

    def datagram_received(self, data, addr):
        print(data)
        parcedpacket = [data.hex()[i:i + 2]
                        for i in range(0, len(data.hex()), 2)]
        UDP_IP = "127.0.0.1"
        UDP_PORT = 52001
        gnuradioheader = ['01', '00', '00', '00', '00', '00', '00', '00']
        if parcedpacket[0:8] == gnuradioheader:
            packet = parcedpacket[8:len(parcedpacket)]
            homeid = packet[0:4]
            srcid = packet[4]
            framecontrol = packet[5:6]
            length = packet[7]
            dst = packet[8]
            command_class = packet[9]
            rest = packet[9:len(packet)-1]
            commands = packet[8:len(packet)]
            data2 = int(round(time.time()))
            query = ("INSERT INTO packet_info "
                     "(home_id, src_id, frame_control, "
                     "length, commands, destination,command_class,timestamp) "
                     "VALUES("
                     "?, "  # home id
                     "?, "  # src id
                     "?, "  # frame control
                     "?, "  # length
                     "?, "  # commands
                     "?, ?,?) "  # destination
                     )
            params = (''.join(homeid), srcid, ''.join(framecontrol),
                      length, ''.join(commands), dst, command_class, data2)
            query_db(query, database='database/database' +
                     str(self._dbkey)+'.db', args=params)


# This tread keeps the thread active


class ThreadListener(threading.Thread):
    def __init__(self, data):
        threading.Thread.__init__(self)
        self._dbkey = data
        self._running = True

    def terminate(self):
        print(self._running)
        self._running = False

    def my_protocol_factory(self):
        return EchoServerProtocol(data=self._dbkey)

    def run(self):
        with app.test_request_context():
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            print("Listening to the Packets ", self._dbkey)
            listen = loop.create_datagram_endpoint(
                self.my_protocol_factory, local_addr=('127.0.0.1', 52002))
            transport, protocol = loop.run_until_complete(listen)
            while self._running == True:
                loop.run_until_complete(listen)
            loop.close()
            return "result"

# This Gets the status of the dashboard


@app.route('/api/status')
def status():
    # get the packet data.

    query = ("CREATE TABLE IF NOT EXISTS network_master(id integer PRIMARY KEY, timestamp VARCHAR(25), location VARCHAR(50));")
    query_db(query, database='database/database.db')
    idfo = session.get('id', '')
    status = session.get('active', 0)
    sessioid = session.get('id', '')
    ab = []
    datares = []
    encruptedcount = []
    timestamp = None
    encrupteddevice = 0
    print(idfo)
    devicecount = 0
    location = ""
    if(idfo != ''):
        database = 'database/database'+str(idfo)+'.db'
        ab = query_db(
            "SELECT count(1) total_packets FROM packet_info", database=database)
        datare = query_db("select * from packet_info", database=database)
        try:
            encruptedcount = query_db(
                "SELECT count(1) encrupted_packets FROM packet_info where command_class = '98'", database=database)
        except Exception:
            pass
        datares = {}
        if(type(datare) != int):
            for i in range(len(datare)):
                if(datare[i][1] not in datares):
                    datares.update({datare[i][1]: 1})
                datares.update({datare[i][1]: datares[datare[i][1]]+1})
        timestamp = query_db("select timestamp from network_master where id = ?",
                             database='database/database.db', args=(idfo,))[0][0]
        location = query_db("select location from network_master where id = ?",
                            database='database/database.db', args=(idfo,))[0][0]
        devicecount = query_db(
            "SELECT count(distinct derivedtable.NewColumn) FROM ( SELECT src_id as NewColumn FROM packet_info UNION SELECT destination as NewColumn FROM packet_info ) derivedtable", database=database)[0][0]
        encrupteddevice = query_db(
            "SELECT count(distinct derivedtable.NewColumn) FROM ( SELECT src_id as NewColumn FROM packet_info where command_class = '98' UNION SELECT destination as NewColumn FROM packet_info where command_class = '98' ) derivedtable", database=database)[0][0]
    response = {
        'status': False if status == 0 else True,
        'id': sessioid,
        'packetcount': ab[0][0] if len(ab) > 0 else 0,
        'encruptedcount': encruptedcount[0][0] if len(encruptedcount) > 0 else 0,
        'data': datares,
        'encrupteddevice': encrupteddevice,
        'devicecount': devicecount,
        'location': location,
        'timestamp': timestamp
    }
    return jsonify(response)

# This api gets the config for the devices
@app.route('/api/getconfig')
def getconfig():
    with open(os.path.join(APP_ROOT, 'database/config.csv')) as f:
        data = csv.reader(f, delimiter=',')
        first_line = True
        places = dict()
        for row in data:
            if not first_line:
                places.update({row[1]: {
                    "id": row[0],
                    "hex": row[1],
                    "devicename": row[2],
                    "manufactuer": row[3]
                }})
            else:
                first_line = False
    response = {
        'status': False if status == 0 else True,
        'id': 1,
        'places': places
    }
    return jsonify(response)


# Starts a new sniff
@app.route('/api/start', methods=['POST'])
def start():
    location = request.json.get('location')
    query = (
        "INSERT INTO network_master (timestamp, location)"
        "VALUES (?, ?);"
    )
    data2 = int(round(time.time()))
    params = (data2, json.dumps(location))
    data = query_db(query, database='database/database.db', args=params)

    print("data = ", data)
    session['active'] = 1
    session['id'] = data
    try:
        t = ThreadListener(data=data)
        t.start()
    except Exception:
        t = ThreadListener(data=data)
        t.start()
    response = {
        'status': 1,
        'id': data
    }
    return jsonify(response)

# Stops a running sniff
@app.route('/api/stop', methods=['POST'])
def stop():
    main_thread = threading.current_thread()
    for t in threading.enumerate():
        if t is not main_thread:
            try:
                t.terminate()
            except Exception:
                pass
    session['active'] = 0
    response = {
        'status': 0,
        'id': 1
    }
    return jsonify(response)

# Not used api
@app.route('/api/getdata', methods=['POST'])
def getdata():
    main_thread = threading.current_thread()
    for t in threading.enumerate():
        if t is not main_thread:
            try:
                t.terminate()
            except Exception:
                pass
    session['active'] = 0
    response = {
        'status': 0,
        'id': 1
    }
    return jsonify(response)

# gets timestamped data for the graphs in dashboard


@app.route('/api/gettimestampdata', methods=['GET'])
def gettimestampdata():
    idfo = session.get('id', '')
    homeid = str(request.args.get('id'))
    database = 'database/database'+str(idfo)+'.db'
    if(homeid != "None"):
        ab = query_db("SELECT * FROM packet_info where home_id = '" +
                      homeid+"'", database=database)
    else:
        ab = query_db("SELECT * FROM packet_info", database=database)
    return jsonify({'homeid': ab})

# activates a specific data for display


@app.route('/api/activate', methods=['GET'])
def activate():
    homeid = str(request.args.get('id'))
    status = session.get('active', 0)
    if(status == 0):
        session['id'] = homeid
    return jsonify({'homeid': homeid})

#  gets previous sniffs


@app.route('/api/previousniffs', methods=['GET'])
def previousniffs():
    database = 'database/database.db'
    ab = query_db("SELECT * FROM network_master", database=database)
    return jsonify({'homeid': ab})

#  fetches the html content for the dashboard.
@app.route('/', defaults={'path': ''})
# @app.route('/<path:path>')
def catch_all(path):
    if app.debug:
        return requests.get('http://localhost:8081/{}'.format(path)).text
    return render_template("index.html")

# query databases


def query_db(query, database, args=(), one=False):
    db = get_db(DATABASE=database)
    with db:
        cur = db.cursor()
        cur.execute(query, args)
        db.commit()
        data = cur.lastrowid
        print(cur.lastrowid)
        rv = cur.fetchall()
        cur.close()
        return (rv[0] if rv else data) if len(rv) == 0 else rv

#  support function for db


def get_db(DATABASE):
    db = sqlite3.connect(os.path.join(APP_ROOT, DATABASE))
    return db

#  db terardown
@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()
