import Vue from "vue";
import "./plugins/bootstrap-vue";
import App from "./App.vue";
import './assets/app.css';
import VueApexCharts from 'vue-apexcharts'
Vue.use(VueApexCharts)
Vue.component('apexchart', VueApexCharts)
Vue.config.productionTip = false;
import VModal from 'vue-js-modal'

Vue.use(VModal)

new Vue({
  render: h => h(App)
}).$mount("#app");
