module.exports = {
    theme: {
        minWidth: {
            '0': '0',
            '1/4': '20%',
            '1/2': '50%',
            '3/4': '75%',
            'full': '100%',
        }
    }
}