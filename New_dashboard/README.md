# DashBoard Readme

![Dashboard Screenshot](/New_dashboard/Dashboard.png "Dashboard Screenshot")

![Dashboard Screenshot](/New_dashboard/Dashboard2.png "Dashboard Screenshot")

* Python: 3.6.3
* Vue.js: 2.5.2
* axios: 0.16.2

# Required

* Ubuntu 18.04 or higher
* GNURadio 3.6 or 3.7 (One with Gr-osmosdr support)
* virtualenv
* bit of terminal knowledge

# Running dashboard

## install front-end
mkdir dist

cd frontend

npm install

npm run build

cd ../backend

virtualenv -p python3 venv 

source venv/bin/activate

pip install -r requirements.txt

pip install redis

pip install celery

## Install redis

sudo apt-get install redis-server

cd ..

mkdir database

FLASK_APP=run.py flask run 


# for development of the dashboard.
mkdir dist

cd frontend

npm install

npm run dev

## open a new terminal and follow below commands

## Install redis

sudo apt-get install redis-server

cd backend

virtualenv -p python3 venv 

source venv/bin/activate

pip install -r requirements.txt

pip install redis

pip install celery

cd ..

mkdir database

FLASK_APP=run.py FLASK_ENV=development flask run 





