# PASSIVE DEVICE & ACTIVITY CLASSIFICATION OF Z-WAVE SMART HOME DEVICES

Zwave is a wireless communication protocol which utilizes low energy radio waves in a mesh network to transmit information. It is largely used in home automation devices and has shown a huge potential in recent years. this project aims to sniff the information from the zwave networks and to demonstrate the possible dangers as well as to expose the vulnerabilities of the home automation system being currently used around the world using the zwave technology. Home automation is a very big market and zwave is being used by many different vendors to create products due to its vast approval from the different countries' governments.

This code is part of the Passive Device & Activity Classification of Z-Wave Smart Home Devices project artifacts. To be submitted for COMP5703 Information Technology Capstone Project for semester 1/2020 to The University of Sydney

# Project Hardware

1) Hackrf one radio

![Hackrf one](https://cdn.sparkfun.com//assets/parts/9/9/5/3/13001-01.jpg)


## Part 1 - GRC

the GRC file for the project is located inside GNU-Radio-Companion/ folder

** more instructions on how to run it inside the folder.

## Part 2 - Dashboard

The dashboard is located inside the New_Dsahboard/ Folder

** more instructions on how to run it inside the folder.

## Part 3 - Machine Learning and Processing

The Machine learning and processing for the project can be found in Data_Analysis/ folder

** more instructions on how to run it inside the folder.

# Team member

1. Sajal Jain (480304346) 
2. SoorajRandhir Singh (480304335) 
3. Jung-Chang Liou (490340293) 
4. Dhit Taksinwarajan (490609396) 



Extra research scripts - 

These scripts can be used in order to research more about zwave using the popular scapy toolkit.

1) Sniffer_Files_and_Scripts - Scapy Radio Files.
